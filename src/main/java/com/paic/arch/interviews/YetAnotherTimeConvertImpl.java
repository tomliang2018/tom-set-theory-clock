package com.paic.arch.interviews;

public class YetAnotherTimeConvertImpl implements TimeConverter {
    private final String LINE_FEED = "\n";

    /**
     * Used to dye a value in a bucket
     */
    enum Dyer {
        Y,
        R;

        public String getColor(boolean exists) {
            return exists ? toString() : "O";
        }
    }

    // an array of dyers that dye the color of a value as specified.
    private final Dyer[][] dyers = {
            // second
            {Dyer.Y},
            // hour
            {Dyer.R, Dyer.R, Dyer.R, Dyer.R},
            {Dyer.R, Dyer.R, Dyer.R, Dyer.R},
            // minute
            {Dyer.Y, Dyer.Y, Dyer.R, Dyer.Y, Dyer.Y, Dyer.R, Dyer.Y, Dyer.Y, Dyer.R, Dyer.Y, Dyer.Y},
            {Dyer.Y, Dyer.Y, Dyer.Y, Dyer.Y},

    };

    @Override
    public String convertTime(String aTime) {
        // parse for hour, minute and second
        String[] times = aTime.split(":");
        int h = Integer.parseInt(times[0]);
        int m = Integer.parseInt(times[1]);
        int s = Integer.parseInt(times[2]);

        final StringBuilder sb = new StringBuilder();
        handleSecond(s, sb, 0, 0);
        sb.append(LINE_FEED);

        handleHour(h, sb);
        sb.append(LINE_FEED);

        handleMinute(m, sb);

        return sb.toString();
    }

    private void handleHour(int h, StringBuilder sb) {
        handleDiv5(h, 4, 4, 1, 2, sb);
    }

    private void handleMinute(int m, StringBuilder sb) {
        handleDiv5(m, 11, 4, 3, 4, sb);
    }

    private void handleSecond(int s, StringBuilder sb, int row, int col) {
        // calc minute val
        final boolean on = s % 2 == 0;
        final String color = dyers[row][col].getColor(on);

        sb.append(color);
    }

    /**
     * A generic method that handles value and the reminder having being divided by 5
     * @param num
     * @param firstRowLen
     * @param secondRowLen
     * @param firstDyerRow
     * @param secondDyerRow
     * @param stringBuilder
     */
    private void handleDiv5(int num, int firstRowLen, int secondRowLen, int firstDyerRow, int secondDyerRow, StringBuilder stringBuilder) {
        final int count = num / 5;
        final int reminder = num % 5;

        // handle 5
        for(int i = 0;i < firstRowLen ;i++) {
            stringBuilder.append(dyers[firstDyerRow][i].getColor(i < count));
        }

        stringBuilder.append(LINE_FEED);

        // handle reminders
        for (int j = 0; j < secondRowLen; j++) {
            stringBuilder.append(dyers[secondDyerRow][j].getColor(j < reminder));
        }
    }

    public static void main(String[] args) {
        System.out.println(new YetAnotherTimeConvertImpl().convertTime("23:59:59"));
        System.out.println(new YetAnotherTimeConvertImpl().convertTime("00:00:00"));
    }
}
